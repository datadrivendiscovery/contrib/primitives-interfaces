#!/bin/sh

dir_to_store_primitives=/opt/jhu_primitives/
find $dir_to_store_primitives -mindepth 1 -delete

version=0.1.0
for suffix in d3m.primitives.graph_matching.sgm_nomination.JHU d3m.primitives.graph_matching.asgm_nomination.JHU d3m.primitives.graph_matching.nearest_neighbor_nomination.JHU d3m.primitives.graph_matching.partial_procrustes.JHU d3m.primitives.data_transformation.laplacian_spectral_embedding.JHU d3m.primitives.link_prediction.data_conversion.JHU d3m.primitives.link_prediction.rank_classification.JHU d3m.primitives.classification.gaussian_classification.JHU d3m.primitives.graph_matching.seeded_graph_matching.JHU d3m.primitives.graph_matching.euclidean_nomination.JHU d3m.primitives.graph_clustering.gaussian_clustering.JHU d3m.primitives.data_preprocessing.largest_connected_component.JHU d3m.primitives.data_transformation.load_graphs.JHU d3m.primitives.data_transformation.adjacency_spectral_embedding.JHU
do
    p=$suffix
    mkdir -p $dir_to_store_primitives/$p/$version
    echo "python -m d3m.index describe -i 4 $p > $dir_to_store_primitives/$p/$version/primitive.json"
    python3 -m d3m.index describe -i 4 $p > $dir_to_store_primitives/$p/$version/primitive.json
done